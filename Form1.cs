﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace HW19_ORBAR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void logButton_Click(object sender, EventArgs e)
        {
            bool sign = false;
            string[] users;
            String username = userBox.Text;
            String password = passBox.Text;
            StreamReader sr = new StreamReader(@"Users.txt");
            string data = sr.ReadToEnd();
            data = data.Replace("\r", "");
            users = data.Split('\n');
            for(int i = 0;i<users.Length;i++)
            {
                if (username == users[i].Split(',')[0] && password == users[i].Split(',')[1])
                {
                    sign = true;
                    break;
                }   
            }
            sr.Close();
            if (sign)
            {
                Calander c = new Calander();
                this.Hide();
                c.ShowDialog();
            
            }
            else
            {
                MessageBox.Show("Wrong username or password!");
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
