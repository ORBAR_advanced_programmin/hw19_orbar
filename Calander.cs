﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace HW19_ORBAR
{
    public partial class Calander : Form
    {
        public string[] arr;
        public Calander()
        {
            openFile();
            InitializeComponent();
        }
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string bDay;
            string name;
            bool sign = false;
            string date = monthCalendar1.SelectionRange.Start.ToShortDateString();
            string day = date.Substring(0,2);
            string month = date.Substring(date.IndexOf("/")+1,2);
            string year = date.Substring(date.LastIndexOf("/")+1);
            date = month + "/" + day + "/" + year;//english day fromat
            for (int i = 0; i < arr.Length; i++)
            {
                name = arr[i].Split(',')[0];
                bDay = arr[i].Split(',')[1];
                if(bDay == date)
                {
                    dateBox.Text = "";
                    dateBox.Text = name + " has a birthday in the current date!";
                    sign = true;
                }
            }
            if(sign == false)
            {
                dateBox.Text = "";
                dateBox.Text = "no one has a birthday in the current date";
            }
        }
        private void openFile()
        {
            StreamWriter sw;
            StreamReader sr;
            string path = Directory.GetCurrentDirectory();
            path = path + @"/MagshimBD.txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                sw = File.CreateText(path);
                sw.Close();
            }
            sr = new StreamReader(path);
            string data = sr.ReadToEnd();
            data = data.Replace("\r", "");
            arr = data.Split('\n');
            sr.Close();
        }
    }
}
